Write-Host "$SCENARIO"
If ((!(Test-Path variable:IsWindows)) -OR ($IsWindows)) {
  #IsWindows does not exist in Windows PowerShell (first check above) and is $True on PowerShell Core / 7 on Windows (second check above)
  $CurrentUser = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent())
  $IsAdmin = $CurrentUser.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
  write-host "This session has elevated Administrator rights => $IsAdmin"
  write-host "Elevated admin is ok because this virtual machine is only ever used for one job and tossed - like a container."
} else {
  write-host "Not running on Windows."
}
write-host "This script is a file in the repository that is called by .gitlab-ci.yml"
write-host "Running in project $env:CI_PROJECT_NAME with results at $env:CI_JOB_URL ($env:CI_JOB_URL)."
write-host "PowerShell and .NET Version Details:"
$PSVersiontable
write-host "GitLab CI Variables are always propagated to PowerShell as environment variables and ONLY FOR the default PowerShell shell they are also propagated to regular PowerShell variables."
write-host "Listing all PowerShell variables:"
dir variable: | format-table Name,Value
write-host "Listing all Environment variables:"
dir env: | format-table Name,Value
